# FOR EACH VIDEO, COUNT COMMENTS.
# SORTED DATA HAS 1 COMMENT PER LINE, ALL VIDEOIDS TOGETHER
# 
# STEP 3. REDUCE videoID and counts by summing the counts into 1 record per video

# open s.csv for reading as a file stream named s
# open r.csv for writing as a file stream named r
s = open("s.csv","r")  
r = open("r.csv", "w") 


# initialize thisKey to an empty string
thisKey= ""

# initialize thisValue to 0
thisValue = 0
highest = 0

# output column headers
r.write('videoID,comments\n')

# for every line in s
for line in s:

  # use the line strip and split methods and assign to a list named data
  data = line.strip().split('\t')

  # assign data list to named variables id and comments
  if len(data) !=2:
    continue
    
  id, comments = data

  # if  id is not equal to thisKey
  if id != thisKey:

    # if thisKey
    if thisKey:

      # output the last key value pair result
      r.write(thisKey+'\t'+str(thisValue)+'\n')
      print(r.write(thisKey+'\t'+str(thisValue)+'\n'))
      thisValue += int(comments)
      if thisValue > highest:
        highest = thisValue
        r.write(thisKey+'\t'+str(highest)+'\n')
    
    
    # start over when changing keys - initialize thisKey to id and thisValue to 0 
    thisKey = id
    thisValue = 0
    highest = 0

  # apply the aggregation function  
  thisValue += int(comments)
  if thisValue > highest:
    highest = thisValue


# output the final entry when done
r.write(thisKey + ',' + str(thisValue)+'\n')
r.write(thisKey+'\t'+str(highest)+'\n')

# close the file streams
s.close()
r.close()
